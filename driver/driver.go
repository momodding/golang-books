package driver

import (
	"database/sql"
	"log"
	"os"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/subosito/gotenv"
)

var db *sql.DB

func init()  {
	gotenv.Load()
}

func logFatal( err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func InitDB() *sql.DB  {
	var err error
	dbName := os.Getenv("DB_NAME")
	// dbHost := os.Getenv("DB_HOST")

	dbUri := fmt.Sprintf("dbname=%s sslmode=disable", dbName) //Build connection string

	// pgUrl, err := pq.ParseURL(dbUri)
	db, err = sql.Open("postgres", dbUri)
	logFatal(err)

	err = db.Ping()
	logFatal(err)

	log.Println(dbUri)

	return db
}