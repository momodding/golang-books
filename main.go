package main

import (
	"database/sql"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"GoApi/driver"
	"GoApi/controller"
)

var db *sql.DB

func main() {

	db = driver.InitDB()

	router := mux.NewRouter()

	controller := controller.Controller{}

	// books = append(books,
	// 	Book{ID: 1, Title: "Golang pointer", Author: "Mr. Golang", Year: "2010"},
	// 	Book{ID: 2, Title: "Goroutines", Author: "Mr. Goroutine", Year: "2011"},
	// 	Book{ID: 3, Title: "Golang router", Author: "Mr. Router", Year: "2012"},
	// 	Book{ID: 4, Title: "Golang concurrency", Author: "Mr. Concurent", Year: "2013"},
	// 	Book{ID: 5, Title: "Golang api", Author: "Mr. Api", Year: "2014"},
	// )
	router.HandleFunc("/books", controller.GetBooks(db)).Methods("GET")
	router.HandleFunc("/books/{id}", controller.GetBook(db)).Methods("GET")
	router.HandleFunc("/books", controller.AddBooks(db)).Methods("POST")
	router.HandleFunc("/books/{id}", controller.UpdateBooks(db)).Methods("PUT")
	router.HandleFunc("/books/{id}", controller.DeleteBooks(db)).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8080", router))
}

