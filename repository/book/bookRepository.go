package bookRepository

import (
	"database/sql"
	"log"
	"GoApi/model"
)

type BookRepository struct {

}

func logFatal( err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func (repo BookRepository) GetBooks(db *sql.DB, book model.Book, books []model.Book) []model.Book  {
	rows, err := db.Query("select * from books")
	logFatal(err)

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
		logFatal(err)

		books = append(books, book)
	}
	return books
}

func (repo BookRepository) GetBook(db *sql.DB, book model.Book, id int) model.Book  {
	rows := db.QueryRow("select * from books where id=$1", id)

	err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
	logFatal(err)

	return book
}

func (repo BookRepository) AddBooks(db *sql.DB, book model.Book) int  {
	err := db.QueryRow("insert into books (title, author, year) values ($1, $2, $3) RETURNING id;", book.Title, book.Author, book.Year).Scan(&book.ID)
	logFatal(err)

	return book.ID
}

func (repo BookRepository) UpdateBooks(db *sql.DB, book model.Book, id int) int64  {
	result, err := db.Exec("update books set title=$1, author=$2, year=$3 where id=$4 RETURNING id;", book.Title, book.Author, book.Year, id)
	logFatal(err)
	resultUpdate, err := result.RowsAffected()

	return resultUpdate
}

func (repo BookRepository) DeleteBooks(db *sql.DB, id int) int64  {
	result, err := db.Exec("DELETE FROM books WHERE id = $1 RETURNING id;", id)
	logFatal(err)
	resultDeleted, err := result.RowsAffected()

	return resultDeleted
}