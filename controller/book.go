package controller

import (
	"database/sql"
	"strconv"
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"GoApi/model"
	"GoApi/repository/book"
)

type Controller struct {

}

var books []model.Book
var db *sql.DB

func logFatal( err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetBooks(db *sql.DB) http.HandlerFunc  {
	return func(w http.ResponseWriter, r *http.Request)  {
		w.Header().Add("Content-Type", "application/json")

		var book model.Book
		books = []model.Book{}
		bookRepo := bookRepository.BookRepository{}

		books = bookRepo.GetBooks(db, book, books)
		json.NewEncoder(w).Encode(books)
	}
}

func (c Controller) GetBook(db *sql.DB) http.HandlerFunc  {
	return func (w http.ResponseWriter, r *http.Request)  {
		w.Header().Add("Content-Type", "application/json")
		var book model.Book
		params := mux.Vars(r)
		bookRepo := bookRepository.BookRepository{}

		id, _ := strconv.Atoi(params["id"])

		book = bookRepo.GetBook(db, book, id)

		json.NewEncoder(w).Encode(book)

		// for _, book := range books {
		// 	if book.ID == id {
		// 		json.NewEncoder(w).Encode(&book)
		// 	}
		// }
	}
}

func (c Controller) AddBooks(db *sql.DB) http.HandlerFunc  {
	return func (w http.ResponseWriter, r *http.Request)  {
		w.Header().Add("Content-Type", "application/json")
		var book model.Book
		var bookID int
		bookRepo := bookRepository.BookRepository{}

		json.NewDecoder(r.Body).Decode(&book)

		bookID = bookRepo.AddBooks(db, book)

		json.NewEncoder(w).Encode(bookID)
		// books = append(books, book)
		// json.NewEncoder(w).Encode(books)
	}
}

func (c Controller) UpdateBooks(db *sql.DB) http.HandlerFunc  {
	return func (w http.ResponseWriter, r *http.Request)  {
		w.Header().Add("Content-Type", "application/json")
		var book model.Book
		params := mux.Vars(r)
		bookRepo := bookRepository.BookRepository{}

		id, _ := strconv.Atoi(params["id"])

		json.NewDecoder(r.Body).Decode(&book)

		resultUpdate := bookRepo.UpdateBooks(db, book, id)

		json.NewEncoder(w).Encode(resultUpdate)

		// for i, item := range books {
		// 	if item.ID == book.ID {
		// 		books[i] = book
		// 	}
		// }

		// json.NewEncoder(w).Encode(books)
	}
}

func (c Controller) DeleteBooks(db *sql.DB) http.HandlerFunc  {
	return func (w http.ResponseWriter, r *http.Request)  {
		w.Header().Add("Content-Type", "application/json")
		params := mux.Vars(r)
		bookRepo := bookRepository.BookRepository{}

		id, _ := strconv.Atoi(params["id"])

		resultDelete := bookRepo.DeleteBooks(db, id)

		// for i, item := range books {
		// 	if item.ID == id {
		// 		books = append(books[:i], books[i+1:]...)
		// 	}
		// }
		json.NewEncoder(w).Encode(resultDelete)
	}
}